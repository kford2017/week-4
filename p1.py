#KIERAN FORED
#9/22/15
#Quiz


# Number 1

def task1():
  x = pickAFile()
  picture = makePicture(x)
  pixels = getPixels(picture)
  h = getHeight(picture)
  w = getWidth(picture)
  show(picture)
  for x in range(0,w):
    for y in range(0,h):
      pixel = getPixelAt(picture,x,y)
      b = getBlue(pixel)
      if(b <= 149):
         setBlue(pixel, 255)
         setRed(pixel, 255)
         setGreen(pixel, 255)
         repaint(picture)
task1() 