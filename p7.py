#KIERAN FORED
#9/22/15
#Quiz
 

# Number 7

def mirrorHalf():
 p = pickAFile()
 pict = makePicture(p)
 pixels = getPixels(pict)
 h = getHeight(pict)
 w = getWidth(pict)
 for y in range(0,h/2):
   for x in range(0,w):
     p = getPixel(pict, x, y)
     z = getPixel(pict, x, h - 1 - y)
     color = getColor(p)
     setColor(z, color)

   repaint(pict)
   show(pict)
mirrorHalf()