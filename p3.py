#KIERAN FORED
#9/22/15
#Quiz


# Number 2

def task2():
  x = pickAFile()
  picture = makePicture(x)
  pixels = getPixels(picture)
  h = getHeight(picture)
  w = getWidth(picture)
  show(picture)
  for x in range(0,w):
    for y in range(0,h):
      pixel = getPixelAt(picture,x,y)
      b = getBlue(pixel)
      r = getRed(pixel)
      g = getGreen(pixel)
      
      setBlue(pixel, b/2 )
      setRed(pixel, r*2)
      setGreen(pixel, g/2)
  repaint(picture)
task2() 